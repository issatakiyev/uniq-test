import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import axios from 'axios';
import moment from "moment";

import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import './App.css'; // or 'antd/dist/antd.less'
import { Layout, Menu, Breadcrumb, Empty, Card } from 'antd';
import { LaptopOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;
const { Content, Sider } = Layout;
class IndexForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stages: [],
      deals: [],
      current_stage: 'NEW',
    }
  }

  componentDidMount() {
    // Get stages
    axios.get(`https://uttest.bitrix24.com/rest/1/mxd5fcd4x0ijzegt/crm.status.list?filter[ENTITY_ID]=DEAL_STAGE`)
      .then(res => {
        const stages = res.data.result;
        this.setState({ stages });
      })
      .catch((error) => {
        console.log("error: ", error)
      })

    // Get deals
    axios.get(`https://uttest.bitrix24.com/rest/1/mxd5fcd4x0ijzegt/crm.deal.list`)
      .then(res => {
        const deals = res.data.result;
        this.setState({ deals });
      })
      .catch((error) => {
        console.log("error: ", error)
      })

  }


  getDeals = (stage) => {
    this.setState({
      current_stage: stage
    });
  }

  render() {
    const { deals, stages, current_stage } = this.state

    const filteredDeals = deals.filter(stage => stage.STAGE_ID.includes(current_stage));

    return (
      <div id="container">
        <Layout>
          <Layout>
            <Sider width={200} className="site-layout-background">
              <Menu
                mode="inline"
                defaultSelectedKeys={['0']}
                defaultOpenKeys={['sub1']}
                style={{ height: '100%', borderRight: 0 }}
              >
                <SubMenu key="sub1" icon={<LaptopOutlined />} title="Stages">
                  {stages.map(({ NAME, STATUS_ID }, index) =>
                    <Menu.Item key={index} onClick={() => this.getDeals(STATUS_ID)}>{NAME}</Menu.Item>
                  )}
                </SubMenu>
              </Menu>
            </Sider>
            <Layout style={{ padding: '0 24px 24px' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>Stages</Breadcrumb.Item>
                <Breadcrumb.Item>Deals</Breadcrumb.Item>
              </Breadcrumb>
              <Content
                className="site-layout-background"
                style={{
                  padding: 24,
                  margin: 0,
                  minHeight: 280,
                }}
              >

                {
                  filteredDeals.length > 0 ? (
                    filteredDeals.map(({
                      TITLE,
                      CURRENCY_ID,
                      OPPORTUNITY,
                      DATE_CREATE,
                      TYPE_ID,
                      COMMENTS,
                      TAX_VALUE
                    }, index) =>
                      <Card key={index} title={TITLE} extra={<span className="deal_label">{TYPE_ID}</span>} style={{ width: 300, marginBottom: "20px", marginRight: "20px", display: "inline-block" }}>
                        <p>
                          <span className="deal_price"> {CURRENCY_ID} {OPPORTUNITY} </span>
                          <span className="deal_tax">Tax: {TAX_VALUE} </span>
                        </p>
                        {/* <p><span className="deal_tax">Tax: {TAX_VALUE} </span></p> */}
                        <p>Details: {COMMENTS ? COMMENTS : " - "}</p>
                        <p className="deal_date"><span >{moment({ DATE_CREATE }).format('DD/MM/YYYY, HH:mm:ss')}</span></p>
                      </Card>
                    )
                  ) : <Empty />
                }

              </Content>
            </Layout>
          </Layout>
        </Layout>

      </div>

    );
  }
}

ReactDOM.render(
  <React.StrictMode>
    <IndexForm />
  </React.StrictMode>,
  document.getElementById('root')
);